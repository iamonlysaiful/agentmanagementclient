# AgentManagementClient

First, run "AegentManagementAPI" project before operate "AgentManagementClient", as "AegentManagementAPI" server-side project.

##Technologies used:
=> Angular 8
=> Bootstrap
=> CSS 
=> Sweetalert etc.

##Some picture For Client-side:

###Agent List
![](./Images/1.PNG)


###New Agent
![](./Images/2.PNG)


###Sweet ALert
![](./Images/3.PNG)