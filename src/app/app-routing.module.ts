import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AddNewAgentFormComponent } from './Components/add-new-agent-form/add-new-agent-form.component';
import { AgentListComponent } from './Components/agent-list/agent-list.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';


const routes: Routes = [
  { path: '', component: DashboardComponent, pathMatch: 'full' },
  { path: 'add-new-agent', component: AddNewAgentFormComponent, pathMatch: 'full' },
  { path: 'agent-list', component: AgentListComponent, pathMatch: 'full' },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
