import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { Agents } from '../Entity/agents';
import { catchError, tap, map } from 'rxjs/operators';
import { DataResult } from '../Entity/data-result';

import swal from 'sweetalert';

let httpHeaders = new HttpHeaders()
  //.set('authorization', 'bearer ' + localStorage.getItem('access_token'))
  .set('Content-Type', 'application/json');

const httpOptions = {
  headers: httpHeaders
};


@Injectable({
  providedIn: 'root'
})

export class AgentsService {

  constructor(private http: HttpClient, public _router: Router) { }

  baseUrl: string = 'https://localhost:44350';


  //=================================Error Handler=========================================================
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', JSON.stringify(error));
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`);
      console.log(JSON.stringify(error.error));
    }
    // return an observable with a user-facing error message
    if (error.message != null || error.message != "") {
      return throwError(error);
    } else {
      return throwError('Something bad happened; please try again later.');
    }

  }
  //=================================Error Handler=========================================================

  getAgent() {
    return this.http.get<DataResult>(this.baseUrl + "/api/agents", httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getAgentByID(id:number) {
    return this.http.get<Agents>(this.baseUrl + "/api/agents/"+id, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }


  saveAgent(obj: Agents) {
    return this.http.post(this.baseUrl + "/api/agents", obj, httpOptions).pipe(
      catchError(this.handleError)
    );
  }
  updateAgent(id:number, obj: Agents) {
    return this.http.put(this.baseUrl + "/api/agents/"+id, obj, httpOptions).pipe(
      catchError(this.handleError)
    );
  }
  deleteAgent(id:number) {
    return this.http.delete(this.baseUrl + "/api/agents/"+id, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

//=================================swal===================================================================
showSuccess(succesMessage: string) {
  swal("Well Done! " + succesMessage, {
    title: "Success!",
    icon: "success",
  });
}

showError(errorMessage: string) {
  swal({
    title: "Error Occured!",
    text: "Oops! " + errorMessage,
    icon: "error",
  })
}


}
