import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewAgentFormComponent } from './add-new-agent-form.component';

describe('AddNewAgentFormComponent', () => {
  let component: AddNewAgentFormComponent;
  let fixture: ComponentFixture<AddNewAgentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewAgentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewAgentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
