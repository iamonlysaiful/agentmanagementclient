import { Component, OnInit } from '@angular/core';
import { AgentsService } from 'src/app/Service/agents.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Agents } from 'src/app/Entity/agents';
import { DataResult } from 'src/app/Entity/data-result';

@Component({
  selector: 'app-add-new-agent-form',
  templateUrl: './add-new-agent-form.component.html',
  styleUrls: ['./add-new-agent-form.component.css']
})
export class AddNewAgentFormComponent implements OnInit {

  agentForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, public api: AgentsService) {
    this.agentForm = this.formBuilder.group({
      BusinessId: [0],
      Code: ['', Validators.required],
      Name: ['', Validators.required],
      SMTPPort: ['', Validators.required],
      Status: ['', Validators.required],
      Email: ['', Validators.required],
      Mobile: ['', Validators.required],
      Phone: ['', Validators.required],
      Street: ['', Validators.required],
      City: ['', Validators.required],
      State: ['', Validators.required],
      Zip: ['', Validators.required]
    });
    if (AddNewAgentFormComponent.businessId != 0) {
      this.getAgentsById(AddNewAgentFormComponent.businessId);
    }
  }

  public static businessId: number = 0;

  ngOnInit() {
    if (AddNewAgentFormComponent.businessId != 0) {
      this.getAgentsById(AddNewAgentFormComponent.businessId);
    }

  }

  save() {
    var data: Agents = this.agentForm.value;
    if (AddNewAgentFormComponent.businessId != 0 ) {
      console.log(data, "put");
      this.api.updateAgent(AddNewAgentFormComponent.businessId, data).subscribe(
        (res: Agents) => {

          this.api.showSuccess("Updated Successfully!");
          this.router.navigate(['agent-list']);
        }, err => {
        });

    } else {
      console.log(data, "post");
      this.api.saveAgent(data).subscribe(
        (res: Agents) => {
          this.api.showSuccess("Saved Successfully!");
          this.router.navigate(['agent-list']);
        }, err => {
        });
    }


  }

  async getAgentsById(val: number) {
    await this.api.getAgentByID(val)
      .subscribe((res: Agents) => {
        this.agentForm.patchValue({
          'BusinessId': res.businessId,
          'Code': res.code,
          'Name': res.name,
          'SMTPPort': res.smtpPort,
          'Status': res.status,
          'Email': res.email,
          'Mobile': res.mobile,
          'Phone': res.phone,
          'Street': res.street,
          'City': res.city,
          'State': res.state,
          'Zip': res.zip,

        });

        console.log(res);
      }, err => {
        console.log(err);
      });
  }

}
