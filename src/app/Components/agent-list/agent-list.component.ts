import { Component, OnInit } from '@angular/core';
import { Agents } from 'src/app/Entity/agents';
import { Router } from '@angular/router';
import { AgentsService } from 'src/app/Service/agents.service';
import { DataResult } from 'src/app/Entity/data-result';
import { AddNewAgentFormComponent } from '../add-new-agent-form/add-new-agent-form.component';

@Component({
  selector: 'app-agent-list',
  templateUrl: './agent-list.component.html',
  styleUrls: ['./agent-list.component.css']
})

export class AgentListComponent implements OnInit {
  listAgents: Agents[] = [];
  
  constructor( private router: Router, public api: AgentsService) { 
    this.getAgents();
  }

  ngOnInit() {
    this.getAgents();
  }

  async getAgents() {
    await this.api.getAgent()
      .subscribe((res: DataResult) => {
        this.listAgents = res.data;
        console.log(res);
      }, err => {
        console.log(err);
      });
  }

  edit(busId:number){
    console.log(busId);
      AddNewAgentFormComponent.businessId = busId;
      this.router.navigate(['add-new-agent']);
  }

  delete(busId:number){
    console.log(busId);
      this.api.deleteAgent(busId).subscribe(
        (res) => {
          this.api.showSuccess("Deleted Successfully!");
          this. ngOnInit();
          }, err => {
        });
  }

}
