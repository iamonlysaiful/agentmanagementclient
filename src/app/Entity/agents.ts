export class Agents {
    businessId: number;
    code: string;
    email: string;
    name: string;
    street: string;
    city: string;
    state: string;
    zip: string;
    country: string;
    mobile: string;
    phone: string;
    contactPerson: string;
    referredBy: string;
    logo: string;
    status: number;
    balance: number;
    loginUrl: string;
    securityCode: string;
    smtpServer: string;
    smtpPort: number;
    sMTPUsername: string;
    smtpPassword: string;
    deleted: boolean;
    createdOnUtc: Date | string;
    updatedOnUtc: Date | string;
    currentBalance: number;
}
