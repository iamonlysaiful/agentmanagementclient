import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddNewAgentFormComponent } from './Components/add-new-agent-form/add-new-agent-form.component';
import { AgentListComponent } from './Components/agent-list/agent-list.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { AgentsService } from './Service/agents.service';

@NgModule({
  declarations: [
    AppComponent,
    AddNewAgentFormComponent,
    AgentListComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule,
    FormsModule, // for ngModel
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
